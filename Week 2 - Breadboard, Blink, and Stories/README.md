### Wemos D1 Mini pinout and general intro info
<img src="/Week 2 - Breadboard, Blink, and Stories/Wemos D1 Mini, pinout and general intro info.jpg" width="800"/>

### Task 1 - Blink asynchronous
<img src="/Week 2 - Breadboard, Blink, and Stories/Task 1 - Blink asynchronous/blink.mp4" width="400"/>

### Task 1 - Blink synchronous
<img src="/Week 2 - Breadboard, Blink, and Stories/Task 1 - Blink synchronous/blink.mp4" width="400"/>

### Task 2 - Toggle Led With Button
<img src="/Week 2 - Breadboard, Blink, and Stories/Task 2 - Toggle Led With Button/Screenshot 1 - led off.jpg" width="400"/>
<img src="/Week 2 - Breadboard, Blink, and Stories/Task 2 - Toggle Led With Button/Screenshot 2 - led on.jpg" width="400"/>

### Problems

Following [https://github.com/esp8266/Arduino#installing-with-boards-manager](https://github.com/esp8266/Arduino#installing-with-boards-manager) was necessary to be able to select "Wemos D1 Mini & R2" board on Windows
