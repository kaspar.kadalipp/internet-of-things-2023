5void setup() {
  Serial.begin(9600);
  pinMode(14, INPUT_PULLUP); // D5
  pinMode(12, OUTPUT); // D6
}

bool LedState = true;
bool prevButtonState = 1;

void loop() {
  // read the input pin:
  int buttonState = digitalRead(14);

  if (buttonState == 1 && prevButtonState == 0){
      LedState = !LedState;
  }

  if (LedState){
    digitalWrite(12, HIGH);
    Serial.println("LED ON");
  }else{
    digitalWrite(12, LOW);
    Serial.println("LED OFF");
  }
  prevButtonState = buttonState;
  delay(1);  // delay in between reads for stability
}
