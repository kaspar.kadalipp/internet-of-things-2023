void setup() {
  pinMode(14, OUTPUT); // D5
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(D5, HIGH);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000); // wait for a second
  
  digitalWrite(D5, LOW);   
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);             
}
