import paho.mqtt.client as mqtt
from random import random
from time import sleep

temperature = 20
increaseTemperature = False

client = mqtt.Client()
client.connect(host="iotgateway", port=1883, keepalive=60)

while True:
    if temperature < 23:
        increaseTemperature = True
    if temperature > 28:
        increaseTemperature = False

    if increaseTemperature:
        temperature += random()
    else:
        temperature -= random()

    client.publish("room/temperature", temperature)
    print("send room/temperature", temperature)
    sleep(1)
