import paho.mqtt.client as mqtt
from time import sleep
import re

airConditioning = "off"
prev = "off"

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("room/temperature")


def on_message(client, userdata, msg):
    global prev
    global airConditioning
    temperature = str(msg.payload.decode('UTF-8'))
    if re.match(temperature, "\d+(\.\d+)?"):
        return
    temperature = float(temperature)
    sendUpdate = False
    if temperature > 25:
        if prev != "on":
            airConditioning = "on"
            prev = "on"
            sendUpdate = True
    else:
        if prev != "off":
            airConditioning = "off"
            prev = "off"
            sendUpdate = True

    if sendUpdate:
        client.publish("room/air-conditioning", airConditioning)
        print(f"Send room/air-conditioning: {airConditioning}")
    #print(f"{msg.topic}, {temperature:.2f}°C, air conditioning {airConditioning}")


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(host="iotgateway", port=1883, keepalive=60)
client.loop_start()

while True:
    sleep(1)
