import paho.mqtt.client as mqtt
from time import sleep
import re

airConditioning = "off"


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("room/air-conditioning")


def on_message(client, userdata, msg):
    print(f"Received {msg.payload.decode('UTF-8')}")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(host="iotgateway", port=1883, keepalive=60)
client.loop_start()

while True:
    sleep(1)