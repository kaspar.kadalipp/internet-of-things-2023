### **Main idea**:

A device that detects mosquitoes flying in the bedroom and shoots them down with lasers

### **Problems/challenges:**
- we don't have infrared cameras
- lasers we have aren't powerful enough to kill a mosquito
- we don't have a means to point the laser accurately at flying objects
- we need to borrow lasers from other teams to create a laser show

### **Implementation possibilities:**
- Use a webcam to detect mosquitoes - wouldn't work in a dark room
- We can mock the location of the mosquito flying in the room
- We can show the general location of the mosquito by doing circles/squares around its location with a laser
- The laser included in our kit is most definitely not powerful enough to fry the mosquito, but it's enough to mock the functionality
- For safety reasons, it should only be active at night - we could connect it to a smartwatch to detect when someone is asleep. Or just detect whether the lights are off

### **Story:**

John is constantly disturbed by mosquitoes while trying to sleep. He's tried everything to get rid of them - from mosquito repellents to mosquito nets - but nothing seemed to work. That's when he realized that he  needed to come up with a more innovative solution."

In an innovative effort to combat the problem of pesky mosquitoes, IoT engineer John has created a device that could revolutionize the way we deal with these tiny insects. With the help of cutting-edge technology, John has built a device that detects mosquitoes flying in his bedroom and shoots them down with lasers.

The device, which John has named "Mosquito Assassin", uses a series of sensors and infrared cameras to detect the presence of mosquitoes in the room. Once the system detects the mosquitoes, it quickly targets them with a laser beam, killing them instantly. The laser is precise enough to hit only the mosquitoes, leaving humans and other creatures unharmed.

This technology could be a game-changer in the fight against mosquito-borne diseases such as malaria, dengue fever, and Zika virus. If we can scale up this technology and make it more affordable, we could potentially save millions of lives around the world.

In conclusion, the Mosquito Assassin is a groundbreaking invention that could change the way we deal with mosquitoes. With its precise targeting and instant killing capabilities, it has the potential to save lives and improve the quality of life for millions of people around the world.
