#### Time spent: 3.5 hours + 1.5h practice session

### Task 1 - Control Led with Http

We used VSMQTT for Visual Studio Code as suggested

<img src="/Week 4 - Story + MQTT Introduction + Simulators/Task 1 - MQTT basics/Screenshot 1.jpg" width="400"/>

### Task 2 - MQTT integration

<img src="/Week 4 - Story + MQTT Introduction + Simulators/Task 2 - MQTT integration/integrator.jpg" width="400"/>
<img src="/Week 4 - Story + MQTT Introduction + Simulators/Task 2 - MQTT integration/mqtt_action-rebuilt.PNG" width="400"/>

### Task 3 - MQTT simulators

Used libraries: **[Paho MQTT Python](https://github.com/eclipse/paho.mqtt.python)**.

The thermometer simulator constantly sends out room temperature measurements. 

The air conditioner simulator listens to messages sent from the thermometer and turns itself on/off at a certain temperature threshold.

<img src="/Week 4 - Story + MQTT Introduction + Simulators/Task 3 - MQTT simulators/Screenshot.jpg" width="600"/>

### Task 4 - MQTT on microcontroller

Replace simulators with actual sensors

**Prerequisites:**

Install **[Dallas Temeperature Sensor library](/Week 4 - Story + MQTT Introduction + Simulators/Task 4 - MQTT on microcontroller/Arduino Library Manager.jpg)** through arduino IDE 

Download **[PubSub Client](https://github.com/knolleary/pubsubclient)** and copy the contents to C:\Users\\...\Documents\Arduino\libraries

<img src="/Week 4 - Story + MQTT Introduction + Simulators/Task 4 - MQTT on microcontroller/Screnshot 2 - ac on.jpg" width="400">
<br/>
<img src="/Week 4 - Story + MQTT Introduction + Simulators/Task 4 - MQTT on microcontroller/video.mp4" height="400">


