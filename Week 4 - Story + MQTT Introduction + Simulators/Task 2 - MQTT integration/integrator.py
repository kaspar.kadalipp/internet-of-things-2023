from iotknit import *

init("iotgateway")  # use a MQTT broker on localhost

prefix("room")  # all actors below are prefixed with /led

switch = publisher("air-conditioning")  # publishes to room/air-conditioning


def tempCallback(msg):
    print("received: [temp]", msg)

    try:
        t = int(msg)
    except ValueError:
        return

    if t >= 25:
        switch.publish("set", "on")  # publish updated state
        print("sending: [r1]", "on")
    else:
        switch.publish("set", "off")
        print("sending: [r1]", "off")


prefix("room")  # all sensors below are prefixed with /button

button1 = subscriber("temperature")  # publishes to room/air-temperature
# subscribes only to button/button1
button1.subscribe_change(callback=tempCallback)

run()  # you can also do a while loop here call process() instead





