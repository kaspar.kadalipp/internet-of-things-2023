#include <ESP8266WiFi.h>
#include <PubSubClient.h>
// Update these with values suitable for your network.

const char* ssid = "iotempire-kadaroosa";
const char* password = "iotempire";
const char* mqtt_server = "iotgateway";

WiFiClient espClient;
PubSubClient client(espClient);

unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
bool value = 0;

int sensor = D5; //sensor pin
int tiltState; //numeric variable

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup()
{
	pinMode(sensor, INPUT); //set sensor pin as input

  Serial.begin(9600);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
}
void loop()
{
  Serial.print("Publishing water level: ");
	tiltState = digitalRead(sensor); //Read the sensor
	if(tiltState == LOW) //when magnetic field is detected, turn led on
	{
     client.publish("tilt-sensor/water-level", "low");
     Serial.println("low");
	}
	else
	{
     client.publish("tilt-sensor/water-level","high");
     Serial.println("high");
	}

  if (!client.connected()) {
    reconnect();
  }
  delay(1000);
}

