## Laser Sensor

**Difficulty: Medium**

Subtracting 80mm 'calibrates' to zero value on touch proximity.Reports smaller values than expected on very short distances.\
Filter is needed for stable measurement. After that it can measure reliably up to 1.2 meters. \
??? Moving boat: multiple sensors to combat waves, similar to ultrasonic distance sensor ???

<img src="/Week 9 - Liquids Measuring/Laser Sensor/Demo.mp4" width="600"/>


## Hall Sensor

**Difficulty: Easy**

Accuracy depends on how powerful the magnet is. Ours can be detected from 1cm away.\
Advantage: magnetic field can be detected through plastic.\
Can only send 0/1 signals, so not much precision here apart from low/high fluid levels. \
??? Moving boat: multiple sensors to combat waves ???

<img src="/Week 9 - Liquids Measuring/Hall Sensor/Demo.mp4" width="600"/>


## Tilt Sensor (Flip Switch)

**Difficulty: Hard**

Figuring out how to measure the fluid was difficult.\
The contraption was suffucient enough for rough measuring.\
Can only send 0/1 signals, so not much precision here apart from low/high fluid levels. \
??? Moving boat: multiple sensors to combat waves ???

<img src="/Week 9 - Liquids Measuring/Tilt Switch/Demo.mp4" width="600"/>


## Ultrasonic Distance Sensor

**Difficulty: Easy**

It's similar to the laser sensor and works with all three liquids. No calibration was needed. It was fairly accurate with different liquid levels. \
Moving boat: Waves hitting the boat can mess with the sensor readings. However if you were to use two (or more to be more accurate) sensors, then you could only consider readings where two sensors had similar values.

<img src="/Week 9 - Liquids Measuring/Ultrasonic Distance Sensor/ultrasonic.png" width="600"/>

## Moisture Sensor

**Difficulty: Easy**

I used 5V and when it was dry it output 1024 so in Node-RED I turned it around (0 means dry). It worked with clean and dirty water, but it didn't work with oil. \
Moving boat: If the boat doesn't turn to its side, then it should work fairly fine. Since the sensor outputs value from 0-1024, it's possible to ignore readings that are from splashes of water. However there probably would still be some margin of error, when liquid surface gets close to the sensor.

<img src="/Week 9 - Liquids Measuring/Moisture Sensor/Demo.mp4" width="300"/>


## Scale

**Difficulty: Medium**

Soldering is required to make wire connections not break easily. When callibrated, the output is +-1g smaller/bigger than the actual weight. \
Moving boat: To weigh something, it needs to sit on top of the scale. I'm not sure how good idea it would be to let some kind of container to move freely on the boat.

<img src="/Week 9 - Liquids Measuring/Scale/Demo.mp4" width="600"/>

## Recommendation report

Scale might be the worst choice, because you can't have containers fly around the ship when waves hit. Also tilt and hall sensors may not be good options, because they only send 0/1 signals. \
With ultrasonic distance and laser sensor, it's possible to use multiple of these to ignore liquid level changes caused by ship tilt.

# Raw Data

| Liquid      | Sensor                     | Difficulty                                                        | Precision                                                                                                                                                                                                                               |
|-------------|----------------------------|-------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Clean water | Ultrasonic distance sensor | Easy                                                              | Fairly precise. There may be some ~8mm diviations when the sensor is moving around.                                                                                                                                                     |
|             | Laser distance sensor      | Moderate (command reference in source code, not in documentation) | Subtracting 80mm 'calibrates' to zero value on touch proximity. Reports smaller values than expected on very short distances. Filter is needed for stable measurement. After that it can measure reliably up to 1.2 meters.             |
|             | weight                     |  Easy                                                                 |      When callibrated, the output is +-1g smaller/bigger than the actual weight.                                                                                                                                                                                                                                    |
|             | Hall sensor                | Easy                                                              | Accuracy depends on how powerful the magnet is. Ours can be detected from 1cm away.  Advantage: magnetic field can be detected through plastic. Can only send 0/1 signals, so not much precision here apart from low/high fluid levels. |
|             | Moisture sensor            | Easy                                                                  |  It gives a value between 0 and 1024. It's possible to processes the value and decide if it's considered wet or dry. Requires some testing to find desirable value for it.                                                                                                                                                                                                                                       |
|             | Tilt sensor (flip switch)  | Hard (figuring out how to measure the fluid was difficult)        | The contraption was suffucient enough for rough measuring. Can only send 0/1 signals, so not much precision here apart from low/high fluid levels.                                                                                      |
| Dirty water | Ultrasonic distance sensor | Easy                                                              | Same as clean water.                                                                                                                                                                                                                    |
|             | Laser distance sensor      | Moderate (command reference in source code, not in documentation) | Same as clean water.                                                                                                                                                                                                                    |
|             | weight                     | Easy                                                                  |           Same as clean water.                                                                                                                                                                                                                              |
|             | Hall sensor                | Easy                                                              | Same as clean water.                                                                                                                                                                                                                    |
|             | Moisture sensor            | Easy                                                                  |      Same as clean water.                                                                                                                                                                                                                                   |
|             | Tilt sensor                | Same as clean water        | Same as clean water.                                                                                                                                                                                                                    |
| Oil         | Ultrasonic distance sensor | Easy                                                              | Same as clean water.                                                                                                                                                                                                                    |
|             | Laser distance sensor      | Moderate (command reference in source code, not in documentation) | Same as clean water.                                                                                                                                                                                                                    |
|             | weight                     | Easy                                                                  |      Same as clean water.                                                                                                                                                                                                                                   |
|             | Hall sensor                | Easy                                                              | Same as clean water.                                                                                                                                                                                                                    |
|             | Moisture sensor           |    Impossible                                                             |         This sensor doesn't detect oil.                                                                                                                                                                                                                                 |
|             | Tilt sensor                | Same as clean water        | Same as clean water.                                                                                                                                                                                                                    |
