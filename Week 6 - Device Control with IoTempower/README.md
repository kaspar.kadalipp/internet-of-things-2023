#### Time spent: 3.5 hours + 1.5h practice session

### Task 1 - More Temperature

- dht22 temperature sensor on ESP32
- results are displayed as a graph in Node-Red dashboard
- threshold for cold and hot is 25°, which are respectively blue and red on the graph

<img src="/Week 6 - Device Control with IoTempower/Task 1 - More Temperature/Screenshot 1 - Node-RED dashboard.png" width="800"/>


### Task 2.1 - Exploring IoTempower Services and Commands

**[Tool support in IoTempower](/Week 6 - Device Control with IoTempower/Task 2.1 - Exploring IoTempower Services and Commands/readme.md#tool-support-in-iotempower)**

**[Documentation on using sensors](/Week 6 - Device Control with IoTempower/Task 2.1 - Exploring IoTempower Services and Commands/readme.md#documentation-on-using-sensors)**

### Task 2.2 - First Node

- send out MQTT request using IoTempower
- listen to requests from terminal

<img src="/Week 6 - Device Control with IoTempower/Task 2.2 - First Node/Screenshot 1 -first deploy.png" width="400"/>
<img src="/Week 6 - Device Control with IoTempower/Task 2.2 - First Node/Screenshot 2 - next deploy.png" width="400"/>

### Task 2.3 - Second Node

- control LED with button press using Node-Red

Notes: ```F2 -> Advanced -> Initialize Serial``` is required for first time deployment

<img src="/Week 6 - Device Control with IoTempower/Task 2.3 - Second Node/Screenshot 1 - Node-RED.png" width="800"/>
<br>
<br>
<img src="/Week 6 - Device Control with IoTempower/Task 2.3 - Second Node/video.mp4" width="600"/>

### Task 2.4 - Button to sound and notification

- button press plays a sound and shows a notification in Node-Red dashboard

<img src="/Week 6 - Device Control with IoTempower/Task 2.4 - Button to sound and notification/Video.mp4" width="600"/>

### Task 2.5 - Text receiver

- display sent MQTT messages on OLED display

<img src="/Week 6 - Device Control with IoTempower/Task 2.5 - Text receiver/Screenshot 1 - oled display.jpg" width="400"/>

### Task 2.6 - RFID reader

- button press requests user to scan a RFID tag
- scanning one of the two tags in the kit shows "Access Granted"
- if no accepted tag is scanned within 10 seconds then "Access Denied" is shown

I was unsure how to accomplish this in Node-Red without relying on Javascript.\
But then I remembered that flows could use global variables, which was the key to getting this to work.

<img src="/Week 6 - Device Control with IoTempower/Task 2.6 - RFID reader/Screenshot 2 - Node-Red.png" width="800"/>
<br>
<img src="/Week 6 - Device Control with IoTempower/Task 2.6 - RFID reader/Screenshot 1 - rfid reader.jpg" width="400"/>
