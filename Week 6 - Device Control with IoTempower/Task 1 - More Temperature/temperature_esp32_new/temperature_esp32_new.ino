#include <WiFi.h>
#include <PubSubClient.h>
#include <DHTesp.h>

// Update these with values suitable for your network.

const char* ssid = "iotempire-kadaroosa";
const char* password = "iotempire";
const char* mqtt_server = "iotgateway";

WiFiClient espClient;
PubSubClient client(espClient);


void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


DHTesp dhtSensor;
int dhtPin = 16;


void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);

  dhtSensor.setup(dhtPin, DHTesp::DHT22);
}

float tempC;
char tempChar[10];

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  tempC = dhtSensor.getTemperature();
  dtostrf(tempC, 2, 3, tempChar);
  
  Serial.print("Publishing temperature: ");
  Serial.println(tempChar);
  client.publish("room2/temperature", tempChar);
  delay(1000);
}


