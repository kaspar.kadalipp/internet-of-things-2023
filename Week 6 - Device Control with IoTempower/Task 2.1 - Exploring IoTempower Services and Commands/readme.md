## Tool support in IoTempower
The tools include:
* previously learned MQTT commands
* ```accesspoint``` - for creating a WiFi hotspot
* ```iot doc serve``` for documentation
* ```iot upgrade``` and ```iot install``` for updating environment
* ```iot menu``` a text based menu for working with microchips with following commands:
* ```console_serial``` - for seeing debug messages of a connected node
* ```initialize``` and ```deploy serial``` - for resetting/flashing the currently connected node
* ```deploy``` - update software of (flash) a node over the network
* ```adopt``` and ```deploy adopt``` - use a connected flashed microcontroller to flash another microcontroller over the network? 

Most of these commands can be called with help (ie. deploy help) to get more information.

## Documentation on using sensors
```iot doc serve``` did not work locally for us, so we gathered the documentation from https://github.com/iotempire/iotempower/blob/master/doc/node_help/commands.rst instead.

Tying to access links didn't work because they had prefix /cloudcmd/fs/home/iot... which should have been just /home/iot... for it to work.

### Example code and important remarks about wiring:

##### [RFID Reader](https://github.com/iotempire/iotempower/blob/master/doc/node_help/mfrc522.rst) - ```mfrc522(name);```

Example: ```mfrc522(reader);```\
Sends data to ```node_topic/reader``` and ```node_topic/reader/uid```


##### [Pulse Width Modulation](https://github.com/iotempire/iotempower/blob/master/doc/node_help/pwm.rst) (PWM) - ```pwm(name, pin, frequency=1000);```

Example: ```pwm(blue, ONBOARDLED, 2000);```

Set values from MQTT:
* Frequency: [nodename]/blue/frequency/set
* Duty cycle: [nodename]/blue/set

##### [Servo motor](https://github.com/iotempire/iotempower/blob/master/doc/node_help/servo.rst) - ```servo(name, pin, min_us=600, max_us=2400, turn_time_ms=700);```
Example: ```servo(m1, D4);```

##### [Ultrasonic distance sensor](https://github.com/iotempire/iotempower/blob/master/doc/node_help/hcsr04.rst) RCWL-1601 - ```hcsr04(name, trigger_pin, echo_pin, timeout_us=30000);```

Example: ```hcsr04(distance, D5, D6).with_precision(10);```\
Wiring - The RCWL-1601 is specified for 3.3V and does not need the voltage divider, do not connect it to 5V.\
On the Wemos D1 Mini, use D5, D6, or D7 pins instead of D1, D2.


##### [Single RGB LED](https://github.com/iotempire/iotempower/blob/master/doc/node_help/rgb_single.rst) - ```rgb_single(name, pin_r, pin_g, pin_b, invert=false);```

Example: ```rgb_single(rgb1, D3, D4, D2);```\
Wiring - integrated RGB module does not need resistors; RGB led with four legs (soldered or on a breadboard) needs 3 100-500 Ohm resistors for R, G and B channels.

