## Task 1 - Analog Touch Sensor

Touching the cable end changes signal strength

<img src="/Week 8 - Analog Sensors/Task 1 - Analog Touch Sensor/Screenshot.png" width="600"/>

## Task 2 - Moisture Sensor


<img src="/Week 8 - Analog Sensors/Task 2 - Moisture Sensor/Screenshot 1 - connections.png" width="600"/>
<br>
<img src="/Week 8 - Analog Sensors/Task 2 - Moisture Sensor/Screenshot 2 - measure moisture.jpg" width="426"/>
