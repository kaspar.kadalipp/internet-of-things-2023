#### Time spent: 6.5 hours + 1.5h practice session

### Task 1 - Node-RED Intro

- Integrator \- listens to temperature reading and sends out mesage to turn on air-conditioning
- Air-conditioner - reacts to messages "on" and "off"

It took a while to realize that text colour is supposed to change in the dashboard, not in the diagram.

<img src="/Week 5 - More Hardware and Integration with Node-RED/Task 1 - Node-RED Intro/Screenshot 1 - AC on.jpg" width="800"/>
<br>
<img src="/Week 5 - More Hardware and Integration with Node-RED/Task 1 - Node-RED Intro/Screenshot 2 - AC off.jpg" width="800"/>

### Task 2 - Emergency Button

- Button press sends a message to #sandbox discord channel

The only tricky part was finding the correct pin for the button, which eventually was D3.

<img src="/Week 5 - More Hardware and Integration with Node-RED/Task 2 - Emergency Button/Screenshot 1 - sent messages.png" width="800"/>

### Task 3 - Remote Control an Internal Device

- Tagged message "on" and "off" to the bot in #sandbox discord channel control the relay

We used strcmp() function to compare string value of the received message

<img src="/Week 5 - More Hardware and Integration with Node-RED/Task 3 - Remote Control an Internal Device/Screenshot 1 - control relay.jpg" width="800"/>
