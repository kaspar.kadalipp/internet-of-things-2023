/**
   BasicHTTPClient.ino

    Created on: 24.05.2015

    Code is identical to task 2
*/

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#include <WiFiClient.h>

ESP8266WiFiMulti WiFiMulti;

void setup() {
  pinMode(14, INPUT_PULLUP);  // D5
  Serial.begin(115200);
  // Serial.setDebugOutput(true);

  Serial.println();
  Serial.println();
  Serial.println();

  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("iotempire-kadaroosa", "iotempire");  // ssid, password
}

bool prevButtonState = 1;

void loop() {
  // wait for WiFi connection
  int buttonState = digitalRead(14);
  if (buttonState == 1 && prevButtonState == 0) {
    if ((WiFiMulti.run() == WL_CONNECTED)) {

      WiFiClient client;
      HTTPClient http;

      if (http.begin(client, "http://192.168.12.55/toggle")) {  // HTTP

        // start connection and send HTTP header
        int httpCode = http.GET();

        // httpCode will be negative on error
        if (httpCode > 0) {
          // file found at server
          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            String payload = http.getString();
            Serial.println("code: " + String(httpCode) + ", " + payload);
          }
        } else {
          Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }

        http.end();
      } else {
        Serial.printf("[HTTP} Unable to connect\n");
      }
    }
  }
  prevButtonState = buttonState;
  delay(1);  // delay in between reads for stability
}
