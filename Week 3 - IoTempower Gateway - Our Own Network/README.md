### Task 1 - Control Led with Http

Modify ESP8266WebServer/HelloServer example to control the builtin LED with /on and /off endpoints.

<img src="/Week 3 - IoTempower Gateway - Our Own Network/Task 1 - Control Led with Http/Screenshot 1 - led off.jpg" width="400"/>
<img src="/Week 3 - IoTempower Gateway - Our Own Network/Task 1 - Control Led with Http/Screenshot 2 - led on.jpg" width="400"/>

### Task 2 - Use a Button to Create Web Request

### Part 1

Modify ESP8266HTTPClient/BasicHttpClient example to blink LED on server using /on and /off endpoints.

<img src="/Week 3 - IoTempower Gateway - Our Own Network/Task 2.1 - Use a Button to Create Web Request/Screenshot 1 - led on.jpg" width="400"/>
<img src="/Week 3 - IoTempower Gateway - Our Own Network/Task 2.1 - Use a Button to Create Web Request/Screenshot 2 - led off.jpg" width="400"/>


### Part 2

Create a new endpoint /toggle to the server that toggles the LED state.

Button press send sends request to /toggle from the client.

<img src="/Week 3 - IoTempower Gateway - Our Own Network/Task 2.2 - Use a Button to Create Web Request/Screenshot 1 - led on.jpg" width="400"/>
<img src="/Week 3 - IoTempower Gateway - Our Own Network/Task 2.2 - Use a Button to Create Web Request/Screenshot 2 - led off.jpg" width="400"/>

### Part 3

Add another identical client together with a button. 

Both clients can control the server LED.

<img src="/Week 3 - IoTempower Gateway - Our Own Network/Task 2.3 - Use a Button to Create Web Request/toggle.mp4">
