### Task 1 - Led
<img src="/Week 7 - Access Control System/Task 1 - Led/Screenshot 1 - Node-RED.jpg" width="600"/>
<img src="/Week 7 - Access Control System/Task 1 - Led/PWM Led.mp4" width="600"/>

### Task 2 - Buzzer
<img src="/Week 7 - Access Control System/Task 2 - Buzzer/Screenshot 1 - Node-RED.jpg" width="600"/>
<img src="/Week 7 - Access Control System/Task 2 - Buzzer/Buzzer.mp4" width="600"/>

### Task 3 - RGB Led

<img src="/Week 7 - Access Control System/Task 3 - RGB Led/Screenshot 1 - Node-RED.jpg" width="600"/>
<img src="/Week 7 - Access Control System/Task 3 - RGB Led/RGB Led.mp4" width="600"/>

### Task 4 - Servo
<img src="/Week 7 - Access Control System/Task 4 - Servo/Screenshot 1 - Node-RED.jpg" width="600"/>
<img src="/Week 7 - Access Control System/Task 4 - Servo/Servo.mp4" width="600"/>

### Task 5 - Project 1

- RGB LED and OLED display show access status
- Accepted RFID cards open the lock for a short period
- Rejected RFID cards trigger the buzzer alarm

<img src="/Week 7 - Access Control System/Task 5 - Project 1/Screenshot 1 - Node-RED.jpg" width="600"/>
<img src="/Week 7 - Access Control System/Task 5 - Project 1/Project.mp4" width="600"/>

